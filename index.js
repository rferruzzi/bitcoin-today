window.addEventListener('DOMContentLoaded', function() {
 // DOM Elements
 const app = document.querySelector('.app');
 const usdViewer = app.querySelector('.amount-usd');
 const eurViewer = app.querySelector('.amount-eur');
 const gbpViewer = app.querySelector('.amount-gbp');
 const brlViewer = app.querySelector('.amount-brl');
 const getButton = app.querySelector('.get');
 const checkboxes = [...app.querySelectorAll('input[type="checkbox"]')];
 const welcome = app.querySelector('.welcome');
 var triggered = false;
 
 // Main function
 async function startApp() {
  try {
   if(!triggered) {
    console.log('Gathering information...');
    renderData(await retrieveData());
   }
   triggered = true;
  } catch(err) {
   console.log(err);
   welcome.textContent = 'It seems like we had a problem... :(';
  }
 }
 // Event Handlers
 getButton.addEventListener('click', startApp);
 checkboxes.forEach(box => box.addEventListener('change', boxChangeHandler));
 
 // Needed functions
 async function retrieveData(){
  const blob = await fetch('https://api.coindesk.com/v1/bpi/currentprice.json');
  const blob_brl = await fetch('https://api.coindesk.com/v1/bpi/currentprice/brl.json');
  const response = await blob.json();
  const response_brl = await blob_brl.json();
  const usd = response.bpi.USD.rate;
  const eur = response.bpi.EUR.rate;
  const gbp = response.bpi.GBP.rate;
  const brl = response_brl.bpi.BRL.rate;
  const data = {usd, eur, gbp, brl};

  return data;
 }

 function renderData(amounts) {
  usdViewer.textContent = amounts.usd;
  eurViewer.textContent = amounts.eur;
  gbpViewer.textContent = amounts.gbp;
  brlViewer.textContent = amounts.brl;
    
  const usdCheck = document.querySelector('#USD'); 
  if(!usdCheck.checked) {
   usdCheck.checked = true;
   usdViewer.parentNode.classList.toggle('hidden');
  }
  welcome.classList.add('hidden');
 }

 function boxChangeHandler(ev) {
  const currency = ev.target.dataset.currency;
  const isChecked = ev.target.checked;
  const viewers = [usdViewer.parentNode, 
                   eurViewer.parentNode, 
                   gbpViewer.parentNode,
                   brlViewer.parentNode
                  ];
  const currentViewer = app.querySelector(`.${currency}`);

  const DisplayManager = () => {
   const shouldTrigger = viewers.every(viewer => viewer.classList.contains('hidden'));
   if(shouldTrigger) {
    welcome.classList.remove('hidden');
   } else {
    welcome.classList.add('hidden');
   }
  }
  currentViewer.classList.toggle('hidden');
  DisplayManager();
  }
});
